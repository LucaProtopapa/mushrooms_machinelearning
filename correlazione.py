#Visualizzazione grafica delle tabelle di contingenza
import pandas as pd
from dython import nominal
import matplotlib.pyplot as plt

data = pd.read_csv('../train.csv')
header = list(data)
print(header)
data.head()

#tabella di contingenza classica
nominal.associations(data, nominal_columns=header, figsize=(15, 15),  theil_u=False, annot = False)

#tabella di contingenza con coefficiente di incertezza
nominal.associations(data, nominal_columns=header, figsize=(15, 15),  theil_u=True, annot = False)
