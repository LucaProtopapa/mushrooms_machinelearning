# Mushrooms_MachineLearning

Un Progetto di Machine Learning orientato alla classificazione della commestibilità di un fungo, basandosi sulle sue features.

Progetto a cura di:
* Protopapa Luca 795071
* Rizzi Giulia 794865

I file `mushroom.csv` e `mushroom_adjusted_reduced.csv` rappresentano i dataset utilizzati per il progetto, in particolare il 'reduced' è il dataset a cui sono state tolte le features difficilmente individuabili dagli utenti.

E' inoltre presente un file `AnalisiDataset.R` all'interno del quale sono stati implementati dei metodi di analisi qualitativa del dataset (in particolare del training set).
Il file `correlazione.py` contiene uno script Python per il calcolo e la visualizzazione di una matrice di contingenza (normale e con coefficiente di incertezza Theil's U).

I file `naiveBayes.R`, `hc_bNetwork.R` ed `idTree.R` rappresentano gli script R contenenti i modelli di machine learning applicati con analisi dei relativi risultati mediante Matrici di Confusione, 10-fold Cross Validation e ROC+AUC (in particolare `idTree.R` contiene due tipologie di alberi decisionali e il modello di clustering  KNN).
I file omonimi con relativa estensione '_reduced' si riferiscono ai medesimi modelli di machine learning applicati al dataset ridotto.

I risultati degli script sopra citati sono reperibili all'interno delle cartelle 'Complete' e 'Reduced' (rispettivamente per il dataset completo e quello ridotto) suddivisi in sottocartelle legate ai modelli implementati, per facilitare la reperibilità di tali risultati.